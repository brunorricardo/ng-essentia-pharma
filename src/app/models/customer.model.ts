export interface Customer {
  name: string;
  phone: string;
  email: string;
  photo: any;
  date_registry: string;
}
