import {Component, Input, OnInit} from '@angular/core';
import {CustomerService} from '../services/customer-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @Input() customer_id: number;

  customer: any = {};
  foto;

  constructor(private customerService: CustomerService, private router: Router) {
  }

  ngOnInit() {
    this.getCustomer(this.customer_id);
  }

  addFile(event: any) {
    let files = event.srcElement.files;
    this.foto = files[0];
    this.customer.photo = this.foto.name;
  }

  salvar(event: Event) {
    event.preventDefault();
    this.customerService.addCustomer(this.customer, this.foto).subscribe(res => {
      if (res.done) {
        this.customer = {};
        this.customerService.getAll();
      }
    });
  }

  editar() {
    this.customerService.editOne(this.customer_id, this.customer, this.foto).subscribe(res => {
      if (res.done) {
        this.customerService.getAll();
        this.router.navigate(['home']);
      } else {
        console.error(res);
      }
    });
  }

  process(event: Event) {
    if (this.customer_id) {
      this.editar();
    } else {
      this.salvar(event);
    }
  }

  formatarFone(){
    console.log(this.customer.phone);
  }

  getCustomer(id) {
    if (id > 0) {
      console.log('ID na rota, buscando cliente com ID ' + id);
      this.customerService.getOne(this.customer_id).subscribe(res => {
        this.customer.name = res.name;
        this.customer.phone = res.phone;
        this.customer.email = res.email;
        this.customer.date_registry = res.date_registry;
        this.customer.photo = res.photo;
      });
    }
  }

}
