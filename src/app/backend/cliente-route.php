<?php


require_once 'Customer.php';

ini_set('display_errors', 1);
error_reporting(E_ALL);

define('PHOTO_DIR', '../photos/');

$data = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

  if (!empty($_POST)) {
    $data = $_POST;
  } else {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata, true);
  }
} else {
  $data = $_GET;
}

switch ($data['action']) {
  case 'add-cliente':

    if ($_FILES) {
      $target_file = PHOTO_DIR . basename($_FILES["file"]["name"]);
      move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
    }

    $dados = json_decode($data['cliente'], true);
    $customer = new Customer(null, $dados['name'], $dados['email'], $dados['phone'], basename($_FILES["file"]["name"]));

    $resp = $customer->persistCustomer();
    echo json_encode($resp);

    break;

  case 'edit-cliente':

    $clienteNovo= json_decode($data['cliente'], true);

    if ($_FILES) {
      $target_file = PHOTO_DIR . basename($_FILES["file"]["name"]);
      move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);

      $clienteAtual['photo'] = $target_file;
    }

    $customer = new Customer();
    $customer = $customer->findById($data['id']);
    $customer = $customer->editCustomer($data['id'], $clienteNovo);

    echo json_encode($customer);

    break;

  case 'get-all':

    $lista = new Customer();
    $lista = $lista->getAll();

    echo json_encode($lista);

    break;

  case 'get-one':

    $customer = new Customer();
    $customer = $customer->findById($data['id']);

    echo json_encode($customer->parseJson());

    break;

  case  'delete-one':

    $customer = new Customer();
    $customer = $customer->findById($data['id']);
    echo json_encode($customer->deleteCustomer());

    break;
}






