import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {Customer} from '../models/customer.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  // allClientes = new BehaviorSubject < Customer[] >;
  allClientes: BehaviorSubject<Customer[]> = new BehaviorSubject(null);


  buscaTodos(): Observable<Customer[]> {
    return this.allClientes.asObservable();
  }

  constructor(private http: HttpClient) {

    this.getAll();

  }

  addCustomer(novo: any, novoFoto: any): Observable<any> {
    const form_data = new FormData();
    form_data.append('file', novoFoto);
    form_data.append('action', 'add-cliente');
    form_data.append('cliente', JSON.stringify(novo));
    return this.http.post<any>('backend/cliente-route.php', form_data);
  }

  getAll(): any {
    this.http.get<Customer[]>('backend/cliente-route.php?action=get-all')
      .subscribe(res => {
        this.allClientes.next(res);
      });
  }

  deleteOne(id): any {
    let data = {action: 'delete-one', id};
    return this.http.post<any>('backend/cliente-route.php', data)
      .subscribe(res => {
        if (res.done) {
          this.getAll();
        } else {
          alert('Erro ao deletar, checar LOG');
        }
      });
  }

  editOne(id: number, customer: any, photo: File): Observable<any> {
    const form_data = new FormData();
    form_data.append('file', photo);
    form_data.append('action', 'edit-cliente');
    form_data.append('cliente', JSON.stringify(customer));
    form_data.append('id', id.toString());
    return this.http.post('backend/cliente-route.php', form_data);
  }

  getOne(id): Observable<Customer> {
    return this.http.get<Customer>(`backend/cliente-route.php?action=get-one&id=${id}`);
  }

}
