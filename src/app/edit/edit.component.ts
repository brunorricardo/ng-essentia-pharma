import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  customer_id;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.customer_id = this.route.snapshot.params.id;
  }

}
