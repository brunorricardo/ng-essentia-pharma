import {Component, OnInit} from '@angular/core';
import {CustomerService} from '../../services/customer-service.service';
import {Observable} from 'rxjs';
import {Customer} from '../../models/customer.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  cadastros: Customer[];

  constructor(private customerService: CustomerService) {
  }

  ngOnInit() {

    // this.getAll();
    this.customerService.buscaTodos().subscribe(allCustomer => {
      this.cadastros = allCustomer;
    });
  }

  delete(id){
    this.customerService.deleteOne(id);
  }

}
